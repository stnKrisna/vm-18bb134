// https://stackoverflow.com/a/6070519
#include <stdio.h>

int main()
{
    int n = 0x1;
    printf(*((char *) &n) ? "little-endian\n" : "big-endian\n");
    return 0;
}
