#pragma once

#include "memory.h"
#include "cpu.h"

typedef struct VM {
    CPU cpu;
    Memory memory;
} VM;

/// Create a new virtual machine context
static VM VM_NewVm(memoryAddress_t memorySize, memoryAddress_t stackSize) {
    VM vm;

    Memory_Init(&vm.memory, memorySize);
    CPU_Init(&vm.cpu, stackSize);

    return vm;
}

/// @brief Free a virtual machine context
/// @param vm Reference to a virtual machine context
/// @return 
static void VM_Free (VM * vm) {
    CPU_Free(&vm->cpu);
    Memory_Free(&vm->memory);
}
