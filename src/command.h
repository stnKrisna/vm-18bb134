#pragma once

#include "cpu.h"
#include "memory.h"
#include "instructions.h"

/// Call the implementation given the instruction structure
/// @param cpu CPU context
/// @param mem Memory context
/// @param instruction instruction context
bool Command_Execute (CPU * cpu, Memory * mem, CPU_DecodeInstruction * instruction);
