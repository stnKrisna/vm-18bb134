#pragma once

#include <stdint.h>

// #define LITTLE_ENDIAN 0
// #define BIG_ENDIAN    1

typedef uint32_t cpuArchInt_t;

typedef cpuArchInt_t programCounter_t;
typedef uint8_t byte_t;
typedef cpuArchInt_t memoryAddress_t;
typedef uint8_t cpuFlag_t;
typedef uint8_t byteOrderIdentifier_t;
typedef uint16_t immediateData_t;
typedef uint32_t instruction_t;
typedef cpuArchInt_t registerVal_t;
