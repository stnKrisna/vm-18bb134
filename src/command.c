#include "command.h"
#include "byteUnit.h"

#define INSTRUCTION_COUNT 255

/// @brief  Type def for command implementation callback function
typedef bool(*commandImplementation)(CPU * cpu, Memory * mem, CPU_DecodeInstruction * instruction);

enum MathOp {
    ADD,
    SUB,
    MUL,
    DIV,
} MathOp;

/// Implementation of load immediate mode
bool Command_LoadImm (CPU * cpu, Memory * mem, CPU_DecodeInstruction * instruction, bool negate) {
    if (negate)
        instruction->data.sImmediate = ~instruction->data.sImmediate;
    
    return CPU_SetRegister(cpu, instruction->targetRegister, instruction->data.sImmediate);
}

/// Implementation of load from register
bool Command_LoadReg (CPU * cpu, Memory * mem, CPU_DecodeInstruction * instruction, bool negate) {
    const char errorTitle[] = "LoadReg failed: %s\n";
    
    uint32_t valInReg = 0;
    
    if (!CPU_GetRegister(cpu, instruction->data.sRegister, &valInReg)) {
        printf(errorTitle, "Invalid data");
        return false;
    }
    
    if (negate)
        valInReg = ~valInReg;
    
    return CPU_SetRegister(cpu, instruction->targetRegister, valInReg);
}

/// @brief Load Dword or QWord to register
/// @param cpu CPU context
/// @param mem Memory context
/// @param instruction Instruction context
/// @param negate Should we negate the data before copy?
/// @return 
bool Command_LoadDwordQWord (CPU * cpu, Memory * mem, CPU_DecodeInstruction * instruction, bool negate) {
    const char errorTitle[] = "LoadDwordQWord failed: %s\n";
    const bool isQWord = instruction->data.sImmediate == 0x10;
    const uint32_t dataSize = isQWord ? QWORD : DWORD;

    if (isQWord) {
        printf(errorTitle, "QWord mode not implemented");
        return false;
    }

    // Assume register is 32bit. Need to perform check if Qword is requested

    // Temporary buffer. Set size to max acceptable data size (Qword)
    byte_t temp[QWORD] = {0};

    if (!Memory_GetSeq(mem, instruction->data.sMemAddr, instruction->data.sMemAddr + dataSize, &temp[0])) {
        printf(errorTitle, "GetSeq failed");
        return false;
    }

    // Don't forget to increment PC by data size after loading. Do this just in case target register is PC
    cpu->pc += dataSize + INSTRUCTION_SIZE;

    if (!CPU_SetRegisterSeq(cpu, instruction->targetRegister, &temp[0], dataSize)) {
        printf(errorTitle, "CPU_SetRegisterSeq failed");
        return false;
    }

    return true;
}

/// @brief Load value from memory to target register using address stored in source register
/// @param cpu CPU context
/// @param mem Memory context
/// @param instruction Instruction context
/// @param negate Should we negate the data before copy?
/// @return True on success, false otherwise
bool Command_LoadPtr (CPU * cpu, Memory * mem, CPU_DecodeInstruction * instruction, bool negate) {
    char errorTitle[] = "LoadPtr failure: %s\n";
    memoryAddress_t sourceAddress = 0;

    if (!CPU_GetRegister(cpu, instruction->data.sRegister, &sourceAddress)) {
        printf(errorTitle, "Failed to get address in register");
        return false;
    }

    instruction->data.sCopyByteCount = instruction->data.sCopyByteCount > CPU_REGISTER_SIZE
        ? CPU_REGISTER_SIZE
        : instruction->data.sCopyByteCount;

    byte_t temp[CPU_REGISTER_SIZE] = {0};
    if (!Memory_GetSeq(mem, sourceAddress, sourceAddress + instruction->data.sCopyByteCount, &temp[0])) {
        printf(errorTitle, "Failed to read data from memory");
        return false;
    }

    if (!CPU_SetRegisterSeq(cpu, instruction->targetRegister, &temp[0], CPU_REGISTER_SIZE)) {
        printf(errorTitle, "Failed to copy data back to registry");
        return false;
    }

    return true;
}

bool Command_LOAD (CPU * cpu, Memory * mem, CPU_DecodeInstruction * instruction) {
    switch (instruction->sourceMode) {
        case CPU_I_SOURCE_IMM:
            return Command_LoadImm(cpu, mem, instruction, false);
        case CPU_I_SOURCE_REG:
            return Command_LoadReg(cpu, mem, instruction, false);
        case CPU_I_SOURCE_IMM_DWORD_QWORD:
            return Command_LoadDwordQWord(cpu, mem, instruction, false);
        case CPU_I_SOURCE_PTR:
            return Command_LoadPtr(cpu, mem, instruction, false);
            
        default:
            printf("LOAD Unimplemented source\n");
            return false;
    }
}

bool Command_LNOT (CPU * cpu, Memory * mem, CPU_DecodeInstruction * instruction) {
    switch (instruction->sourceMode) {
        case CPU_I_SOURCE_IMM:
            return Command_LoadImm(cpu, mem, instruction, true);
        case CPU_I_SOURCE_REG:
            return Command_LoadReg(cpu, mem, instruction, true);
            
        default:
            printf("Unimplemented ");
            return false;
    }
    
    return true;
}

uint32_t Command_PerformMath (uint32_t a, uint32_t b, enum MathOp operator) {
    const char errorTitle[] = "Command_PerformMath failed: %s\n";
    
    switch (operator) {
        case ADD:
            a += b;
            break;
            
        case SUB:
            a -= b;
            break;
            
        case MUL:
            a *= b;
            break;
            
        case DIV:
            a /= b;
            break;
            
        default:
            printf(errorTitle, "Invalid math op");
            return false;
    }
    
    return a;
}

/// Implementation of add immediate mode
bool Command_MathOpImm (CPU * cpu, Memory * mem, CPU_DecodeInstruction * instruction, enum MathOp operator) {
    const char errorTitle[] = "Command_MathOpImm failed: %s\n";
    uint32_t valInReg = 0;
    
    if (!CPU_GetRegister(cpu, instruction->targetRegister, &valInReg)) {
        printf(errorTitle, "Invalid data");
        return false;
    }
    
    valInReg = Command_PerformMath(valInReg, instruction->data.sImmediate, operator);
    
    if (!CPU_SetRegister(cpu, instruction->targetRegister, valInReg)) {
        printf(errorTitle, "Invalid data");
        return false;
    }
    
    return true;
}

/// Implementation of load from register
bool Command_MathOpReg (CPU * cpu, Memory * mem, CPU_DecodeInstruction * instruction, enum MathOp operator) {
    const char errorTitle[] = "Command_MathOpReg failed: %s\n";
    
    uint32_t valInRegA = 0;
    uint32_t valInRegB = 0;
    
    bool successGetRegA = CPU_GetRegister(cpu, instruction->targetRegister, &valInRegA);
    bool successGetRegB = CPU_GetRegister(cpu, instruction->data.sRegister, &valInRegB);
    
    if (!(successGetRegA && successGetRegB)) {
        printf(errorTitle, "Invalid data");
        return false;
    }
    
    valInRegA = Command_PerformMath(valInRegA, valInRegB, operator);
    
    if (!CPU_SetRegister(cpu, instruction->targetRegister, valInRegA)) {
        printf(errorTitle, "Invalid data");
        return false;
    }
    
    return true;
}

bool Command_MathOpsFromSrc (CPU * cpu, Memory * mem, CPU_DecodeInstruction * instruction, enum MathOp operator) {
    switch (instruction->sourceMode) {
        case CPU_I_SOURCE_IMM:
            return Command_MathOpImm(cpu, mem, instruction, operator);
        case CPU_I_SOURCE_REG:
            return Command_MathOpReg(cpu, mem, instruction, operator);
            
        default:
            printf("Unimplemented ");
            return false;
    }
    
    return true;
}

bool Command_ADD (CPU * cpu, Memory * mem, CPU_DecodeInstruction * instruction) {
    return Command_MathOpsFromSrc(cpu, mem, instruction, ADD);
}

bool Command_SUB (CPU * cpu, Memory * mem, CPU_DecodeInstruction * instruction) {
    return Command_MathOpsFromSrc(cpu, mem, instruction, SUB);
}

bool Command_MUL (CPU * cpu, Memory * mem, CPU_DecodeInstruction * instruction) {
    return Command_MathOpsFromSrc(cpu, mem, instruction, MUL);
}

bool Command_DIV (CPU * cpu, Memory * mem, CPU_DecodeInstruction * instruction) {
    return Command_MathOpsFromSrc(cpu, mem, instruction, DIV);
}

/// Implementation of jump immediate mode
bool Command_JmpImm (CPU * cpu, Memory * mem, CPU_DecodeInstruction * instruction) {
    cpu->pc = instruction->data.sImmediate;
    return true;
}

/// Implementation of jump from register
bool Command_JmpReg (CPU * cpu, Memory * mem, CPU_DecodeInstruction * instruction) {
    const char errorTitle[] = "Command_Jmp0Reg failed: %s\n";
    
    uint32_t valInReg = 0;
    
    if (!CPU_GetRegister(cpu, instruction->data.sRegister, &valInReg)) {
        printf(errorTitle, "Invalid data");
        return false;
    }
    
    cpu->pc = valInReg;
    
    return true;
}

/// @brief Implementation for jump using Dword or Qword
/// @param cpu CPU context
/// @param mem Memory context
/// @param instruction Instruction context
/// @return True on success
bool Command_JumpDwordQword (CPU * cpu, Memory * mem, CPU_DecodeInstruction * instruction) {
    instruction->targetRegister = CPU_PC;
    if (!Command_LoadDwordQWord(cpu, mem, instruction, false)) {
        printf("Command_JumpDwordQword failed. Failed loading Word type to PC\n");
        return false;
    }

    return true;
}

/// @brief Implementation for jump using value from memory
/// @param cpu CPU context
/// @param mem Memory context
/// @param instruction Instruction context
/// @return True on success
bool Command_JumpMem (CPU * cpu, Memory * mem, CPU_DecodeInstruction * instruction) {
    printf("Command_JumpMem not yet implemented");
    return false;
}

/// @brief Implementation for jump using ptr
/// @param cpu CPU context
/// @param mem Memory context
/// @param instruction Instruction context
/// @return True on success
bool Command_JumpPtr (CPU * cpu, Memory * mem, CPU_DecodeInstruction * instruction) {
    printf("Command_JumpPtr not yet implemented");
    return false;
}

bool Command_JMP0 (CPU * cpu, Memory * mem, CPU_DecodeInstruction * instruction) {
    const char errorTitle[] = "Command_JMP0 failed: %s\n";
    
    uint32_t valInReg = 0;
    
    if (!CPU_GetRegister(cpu, instruction->targetRegister, &valInReg)) {
        printf(errorTitle, "Invalid data");
        return false;
    }
    
    if (valInReg == 0) {
        switch (instruction->sourceMode) {
            case CPU_I_SOURCE_IMM:
                return Command_JmpImm(cpu, mem, instruction);
            case CPU_I_SOURCE_REG:
                return Command_JmpReg(cpu, mem, instruction);
            case CPU_I_SOURCE_IMM_DWORD_QWORD:
                return Command_JumpDwordQword(cpu, mem, instruction);
            case CPU_I_SOURCE_MEM:
                return Command_JumpMem(cpu, mem, instruction);
            case CPU_I_SOURCE_PTR:
                return Command_JumpPtr(cpu, mem, instruction);
                
            default:
                printf("Unimplemented ");
                return false;
        }
    }
    
    return true;
}

bool Command_JMP (CPU * cpu, Memory * mem, CPU_DecodeInstruction * instruction) {
    switch (instruction->sourceMode) {
        case CPU_I_SOURCE_IMM:
            return Command_JmpImm(cpu, mem, instruction);
        case CPU_I_SOURCE_REG:
            return Command_JmpReg(cpu, mem, instruction);
        case CPU_I_SOURCE_IMM_DWORD_QWORD:
            return Command_JumpDwordQword(cpu, mem, instruction);
        case CPU_I_SOURCE_MEM:
            return Command_JumpMem(cpu, mem, instruction);
        case CPU_I_SOURCE_PTR:
            return Command_JumpPtr(cpu, mem, instruction);
            
        default:
            printf("Unimplemented\n");
            return false;
    }
    
    return true;
}

bool Command_OUTC (CPU * cpu, Memory * mem, CPU_DecodeInstruction * instruction) {
    const char errorTitle[] = "Command_OUTC failed: %s\n";
    
    uint32_t valInReg = 0;
    
    if (!CPU_GetRegister(cpu, instruction->data.sRegister, &valInReg)) {
        printf(errorTitle, "Invalid data");
        return false;
    }
    
    switch ((uint8_t)instruction->targetRegister) {
        case 0:
            printf("%c", (uint8_t)valInReg);
            break;
            
        case 1:
            printf("%d", valInReg);
            break;
            
        default:
            break;
    }
    
    return true;
}

/// @brief Implementation of the GETC command
/// @param cpu 
/// @param mem 
/// @param instruction 
/// @return 
bool Command_GETC (CPU * cpu, Memory * mem, CPU_DecodeInstruction * instruction) {
    int userChar;

    // Get character while user input is not a number
    while ((userChar = getchar()) < '0' && userChar > '9');

    // Purge junk
    int junk;
    while ((junk = getchar()) != '\n' && junk != EOF /* && junk != '\0' */);

    if (!CPU_SetRegister(cpu, instruction->targetRegister, (byte_t)userChar)) {
        printf("Command_GETC failed. Failed to set register");
        return false;
    }

    return true;
}

/// @brief LOADM implementation. Load register content to memory
/// @param cpu CPU context
/// @param mem Memory context
/// @param instruction Instruction context
/// @return True on success. False on failure
bool Command_LOADM (CPU * cpu, Memory * mem, CPU_DecodeInstruction * instruction) {
    const char errorTitle[] = "Command_LOADM failed: %s\n";

    uint32_t valInReg = 0;
    
    if (!CPU_GetRegister(cpu, instruction->data.sRegister, &valInReg)) {
        printf(errorTitle, "Invalid data");
        return false;
    }

    // Assume next instruction is memory address
    memoryAddress_t storageAddress = 0;
    const memoryAddress_t nextInstAddress = cpu->pc + INSTRUCTION_SIZE;
    if (!Memory_GetInt32(mem, nextInstAddress, &storageAddress, cpu->endian)) {
        printf(errorTitle, "Failed to get distination address");
        return false;
    }

    // Copy data over to memory
    if (!Memory_SetInt32(mem, storageAddress, valInReg, cpu->endian)) {
        printf(errorTitle, "Failed to copy data from register to memory");
        return false;
    }

    return true;
}

/// @brief No operation
/// @param cpu 
/// @param mem 
/// @param instruction 
/// @return Always true
bool Command_NOOP (CPU * cpu, Memory * mem, CPU_DecodeInstruction * instruction) {
    return true;
}

/// @brief Handler for invalid command
/// @param cpu 
/// @param mem 
/// @param instruction 
/// @return 
bool Command_Invalid (CPU * cpu, Memory * mem, CPU_DecodeInstruction * instruction) {
    return false;
}

/// @brief Perform function call
/// @param cpu CPU context
/// @param mem Memory context
/// @param instruction Instruction context
/// @return True on success
bool Command_FuncCall (CPU * cpu, Memory * mem, CPU_DecodeInstruction * instruction) {
    char errorTitle[] = "Command_FuncCall failed: %s\n";
    /*
    Steps to perform function call:
    1. LR should be pushed to stack
    2. LR should be set to address of the next instruction
    3. PC should be set to target address
    */

    // Step 1
    if (!CPU_PushStack(cpu, CPU_LR)) {
        printf(errorTitle, "Failed to push LR to stack");
        return false;
    }

    // Step 2
    if (!CPU_SetRegister(cpu, CPU_LR, cpu->pc + INSTRUCTION_SIZE)) {
        printf(errorTitle, "Failed to set return address");
        return false;
    }

    // Step 3
    bool pcReplaced = true;
    switch (instruction->sourceMode) {
    case CPU_I_SOURCE_IMM:
        pcReplaced = Command_LoadImm(cpu, mem, instruction, false);
        break;

    case CPU_I_SOURCE_IMM_DWORD_QWORD:
        pcReplaced = Command_LoadDwordQWord(cpu, mem, instruction, false);
        break;
    
    case CPU_I_SOURCE_PTR:
        pcReplaced = Command_LoadPtr(cpu, mem, instruction, false);
        break; 
   
    case CPU_I_SOURCE_REG:
        pcReplaced = Command_LoadReg(cpu, mem, instruction, false);
        break;

    case CPU_I_SOURCE_MEM: {
        memoryAddress_t sAddr = 0;
        pcReplaced = Memory_GetInt32(mem, cpu->pc + INSTRUCTION_SIZE, &sAddr, cpu->endian);
        pcReplaced = CPU_CopyMemToReg(cpu, mem, sAddr, CPU_PC, CPU_ARCHITECTURE == 64 ? (8 * BYTE) : (4 * BYTE));
        break;
    }
    
    default:
        pcReplaced = false;
        break;
    }

    if (!pcReplaced) {
        printf(errorTitle, "Failed to set program counter");
    }

    return pcReplaced;
}

bool Command_FuncReturn (CPU * cpu, Memory * mem, CPU_DecodeInstruction * instruction) {
    char errorTitle[] = "Command_FuncReturn failed: %s\n";
    /*
    Steps to perform function return:
    1. Copy LR to PC
    2. Pop stack to LR
    */

    // Step 1
    if (!CPU_CopyBetweenRegister(cpu, CPU_LR, CPU_PC)) {
        printf(errorTitle, "Failed to copy value between register");
        return false;
    }

    // Step 2
    if (!CPU_PopStack(cpu, CPU_LR)) {
        printf(errorTitle, "Failed to pop stack to LR");
        return false;
    }

    return true;
}

bool Command_Coroutines (CPU * cpu, Memory * mem, CPU_DecodeInstruction * instruction) {
    if (instruction->flag == CPU_COROUTINE_PUSH) {
        return CPU_PushStack(cpu, instruction->data.sRegister);
    } else if (instruction->flag == CPU_COROUTINE_POP) {
        return CPU_PopStack(cpu, instruction->targetRegister);
    } else if (instruction->flag == CPU_COROUTINE_FUNC) {
        return Command_FuncCall(cpu, mem, instruction);
    } else if (instruction->flag == CPU_COROUTINE_RETURN) {
        return Command_FuncReturn(cpu, mem, instruction);
    }
    
    printf("Command_Coroutines Flag not implemented\n");
    
    return false;
}

bool Command_Execute (CPU * cpu, Memory * mem, CPU_DecodeInstruction * instruction) {
    switch (instruction->opcode) {
        case CPU_I_LOAD:
            return Command_LOAD(cpu, mem, instruction);
        case CPU_I_ADD:
            return Command_ADD(cpu, mem, instruction);
        case CPU_I_SUB:
            return Command_SUB(cpu, mem, instruction);
        case CPU_I_MUL:
            return Command_MUL(cpu, mem, instruction);
        case CPU_I_DIV:
            return Command_DIV(cpu, mem, instruction);
        case CPU_I_JMP_0:
            return Command_JMP0(cpu, mem, instruction);
        case CPU_I_JMP:
            return Command_JMP(cpu, mem, instruction);
        case CPU_I_OUTC:
            return Command_OUTC(cpu, mem, instruction);
        case CPU_I_GETC:
            return Command_GETC(cpu, mem, instruction);
        case CPU_I_NOOP:
            return Command_NOOP(cpu, mem, instruction);
        case CPU_I_LOADM:
            return Command_LOADM(cpu, mem, instruction);
        case CPU_I_COROUTINE_SET:
            return Command_Coroutines(cpu, mem, instruction);
        default:
            return Command_Invalid(cpu, mem, instruction);
    }
}