#include "memory.h"
#include "instructions.h"

#define PRINT_MEMORY_ERROR
#define ALLOW_MEMORY_DUMP

void Memory_Init (Memory * memory, memoryAddress_t virtualMemorySize) {
    memory->used = 0;
    memory->size = virtualMemorySize;
    memory->bytes = safe_malloc(sizeof(byte_t) * memory->size);
    
    // Make sure the first instruction is the halt instruction so that the CPU
    // isnt in an infinite loop
    memory->bytes[0] = 0xfe;
    memory->bytes[1] = 0xe1;
    memory->bytes[2] = 0x0b;
    memory->bytes[3] = 0xed;
}

void Memory_Free (Memory * memory) {
    safe_free(memory->bytes);
    memory->size = memory->used = 0;
}

bool Memory_Get (Memory * memory, memoryAddress_t at, byte_t * content) {
    if (at >= 0 && at < memory->size) {
        (*content) = memory->bytes[at];
        return true;;
    }
    
    return false;
}

/// @brief Perform arbitrary memory data length check
/// @param memory Memory context
/// @param at Start address
/// @param size Length/size of data
/// @param startIndexInRange Store result for start address check if not null
/// @param addressCanFitInt Store result for end address check if not null
/// @return True if size check pass. False on fail
bool Memory_SizeCheck (Memory * memory, memoryAddress_t at, memoryAddress_t size, bool * startIndexInRange, bool * endAddressInMemRange) {
    bool startOk = at >= 0 && at < memory->size;
    bool endOk = at + size <= memory->size;

    if (startIndexInRange != 0)
        *startIndexInRange = startOk;
    
    if (endAddressInMemRange != 0)
        *endAddressInMemRange = endOk;

    return startOk && endOk;
}

bool Memory_GetLength (Memory * memory, memoryAddress_t at, memoryAddress_t length, byte_t * buffer) {
    bool startIndexInRange, endIndexInRange;
    Memory_SizeCheck(memory, at, length, &startIndexInRange, &endIndexInRange);
    
    if (startIndexInRange && endIndexInRange) {
        for (uint32_t i = 0; i < length; ++i) {
            Memory_Get(memory, at + i, &buffer[i]);
        }
        return true;
    }
    
    return false;
}

bool Memory_SetSeq (Memory * memory, memoryAddress_t storeAt, byte_t * dataBuffer, memoryAddress_t dataLength) {
    char errorTitle[] = "Error coppying buffer to memory: %s\n";
    if (!Memory_SizeCheck(memory, storeAt, dataLength, 0, 0)) {
        printf(errorTitle, "Address out of range");
        return false;
    }

    for (uint32_t i = 0; i < dataLength; ++i) {
        if (!Memory_Set(memory, storeAt + i, dataBuffer[i])) {
            printf(errorTitle, "Memory_Set failure");
            return false;
        }
    }

    return true;
}

memoryAddress_t truncDataLength (memoryAddress_t start, memoryAddress_t size, memoryAddress_t dataLength) {
    // Check if the end address can fit in memory
    const memoryAddress_t endAddr = start + dataLength;

    if (endAddr > size) {
        dataLength = size - start;
    }

    return dataLength;
}

bool Memory_SetLengthTrunc (Memory * memory, memoryAddress_t storeAt, byte_t * dataBuffer, memoryAddress_t dataLength) {
    return Memory_SetSeq(
        memory,
        storeAt,
        dataBuffer,
        truncDataLength(storeAt, memory->size, dataLength)
    );
}

bool Memory_GetSeq (Memory * memory, memoryAddress_t start, memoryAddress_t end, byte_t * buffer) {
    uint32_t length = (end + 1) - start;

    if (start > end) {
        printf("Memory_GetSeq failed. end address <less than> start address\n");
        return false;
    }
    
    return Memory_GetLength(memory, start, length, buffer);
}

bool Memory_Set (Memory * memory, memoryAddress_t at, byte_t data) {
    if (at >= 0 && at < memory->size) {
        memory->bytes[at] = data;
        return true;;
    }
    
    return false;
}

/// @brief Perform bound checking for memory RW for 32bit size
/// @param memory Memory context
/// @param at Start address
/// @param startIndexInRange Reference to variable that will hold value if the start index is OK
/// @param addressCanFitInt Reference to variable that will hold value if the size is OK
void Memory_Uint32SizeCheck (Memory * memory, memoryAddress_t at, bool * startIndexInRange, bool * addressCanFitInt) {
    Memory_SizeCheck(memory, at, sizeof(uint32_t), startIndexInRange, addressCanFitInt);
}

/// Print error message for memory read/write
void Memory_PrintRWError (Memory * memory, memoryAddress_t at, bool startIndexInRange, bool addressCanFitInt) {
#ifdef PRINT_MEMORY_ERROR
    printf("Unable to access 0x%x with 4 bytes of data\n", at);

    if (!startIndexInRange) {
        printf("    Memory start address is out of range\n");
    }

    if (!addressCanFitInt) {
        printf("    Requested storage size is out of range\n");
    }

    Memory_PrintMem(memory->bytes, memory->size);
#endif
}

bool Memory_GetInt32 (Memory * memory, memoryAddress_t at, uint32_t * data, int endian) {
    bool startIndexInRange;
    bool addressCanFitInt;
    Memory_Uint32SizeCheck(memory, at, &startIndexInRange, &addressCanFitInt);
    
    if (startIndexInRange && addressCanFitInt) {
        (*data) = CPU_ToVirtualCpuEndian(endian, (*(uint32_t *)(&memory->bytes[at])) );
        
        return true;
    }

    // Failed to read int32 to memory
    Memory_PrintRWError(memory, at, startIndexInRange, addressCanFitInt);
    
    return false;
}

bool Memory_SetInt32 (Memory * memory, memoryAddress_t at, uint32_t data, int endian) {
    bool startIndexInRange;
    bool addressCanFitInt;
    Memory_Uint32SizeCheck(memory, at, &startIndexInRange, &addressCanFitInt);
    
    // Attempt to write to memory
    if (startIndexInRange && addressCanFitInt) {
        typeof(data) value = CPU_ToHostEndian(endian, data);

        memcpy(&memory->bytes[at], (byte_t *)&value, sizeof(typeof(data)));
        return true;
    }

    // Failed to write int32 to memory
    Memory_PrintRWError(memory, at, startIndexInRange, addressCanFitInt);
    
    return false;
}

void Memory_PrintMem (byte_t * bytes, size_t bytesCount) {
#ifdef ALLOW_MEMORY_DUMP
    // Dump bytes and its plaintext representation.
    // Each line should contain 16 bytes

    const size_t lineCount = 1 + ((bytesCount - 1) / 16);
    size_t byteOffset = 0;
    size_t charOffset = 0;

    // Print line
    printf("           0  1  2  3  4  5  6  7    8  9  a  b  c  d  e  f\n"); // Hardcode header lol
    for (size_t line = 0; line < lineCount; ++line) {
        printf("%08zx  ", line * 16);

        // Print byte
        for (size_t i = 0; i < 16; i++) {
            if (byteOffset < bytesCount)
                printf("%02x ", bytes[byteOffset]);
            else
                printf(" . ");

            if (i == 7)
                printf("  ");

            byteOffset++;
        }

        printf("  ");

        // Print byte ASCII
        for (size_t i = 0; i < 16; i++) {
            if (charOffset < bytesCount) {
                if (bytes[charOffset] >= ' ' && bytes[charOffset] <= '~') {
                    printf("%c", bytes[charOffset]);
                } else {
                    printf(".");
                }
            }

            if (i == 7)
                printf(" ");

            charOffset++;
        }

        printf("\n");
    }
#endif
}

bool Memory_Copy (Memory * src, memoryAddress_t sAddress, Memory * dst, memoryAddress_t dAddr, size_t size) {
    bool startIndexInRange, endIndexInRange;
    char errorTitle[] = "Memory_Copy failed: %s\n";

    // Size check src
    if (!Memory_SizeCheck(src, sAddress, size, &startIndexInRange, &endIndexInRange)) {
        printf(errorTitle, "Address out of bound for source memory");
        Memory_PrintRWError(src, sAddress, startIndexInRange, endIndexInRange);
        return false;
    }

    // Size check dst
    if (!Memory_SizeCheck(dst, dAddr, size, &startIndexInRange, &endIndexInRange)) {
        printf(errorTitle, "Address out of bound for target memory");
        Memory_PrintRWError(dst, dAddr, startIndexInRange, endIndexInRange);
        return false;
    }

    // Copy data
    if (memcpy(&dst->bytes[dAddr], &src->bytes[sAddress], size) == 0) {
        printf(errorTitle, "memcpy failed!!!");
        return false;
    }

    return true;
}

bool Memory_CopyTrunc (Memory * src, memoryAddress_t sAddress, Memory * dst, memoryAddress_t dAddr, size_t size) {
    const size_t sSize = truncDataLength(sAddress, src->size, size);
    const size_t dSize = truncDataLength(dAddr, dst->size, size);
    const size_t finalSize = sSize < dSize
        ? sSize
        : dSize;

    return Memory_Copy(
        src,
        sAddress,
        dst,
        dAddr,
        finalSize
    );
}
