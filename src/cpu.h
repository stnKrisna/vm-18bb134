#pragma once

#include "types.h"
#include "utils.h"
#include "memory.h"
#include "byteUnit.h"

/// Count of addressable register
#define CPU_REGISTER_COUNT 16

/// Number of bytes allocated for each register
#define CPU_REGISTER_SIZE  (4 * BYTE)
#define VIRTUAL_CPU_BYTE_ORDER LITTLE_ENDIAN


#define CPU_ARCHITECTURE 32 // BIT

#define hostByteOrder() (endian())
#define shouldConvertToLittleEndian() (hostByteOrder() != VIRTUAL_CPU_BYTE_ORDER)

typedef enum CPU_Register {
    CPU_REG_0 = 0 * CPU_REGISTER_SIZE,
    CPU_REG_1 = 1 * CPU_REGISTER_SIZE,
    CPU_REG_2 = 2 * CPU_REGISTER_SIZE,
    CPU_REG_3 = 3 * CPU_REGISTER_SIZE,
    CPU_REG_4 = 4 * CPU_REGISTER_SIZE,
    CPU_REG_5 = 5 * CPU_REGISTER_SIZE,
    CPU_REG_6 = 6 * CPU_REGISTER_SIZE,
    CPU_REG_7 = 7 * CPU_REGISTER_SIZE,
    CPU_REG_8 = 8 * CPU_REGISTER_SIZE,
    CPU_REG_9 = 9 * CPU_REGISTER_SIZE,
    CPU_REG_10 = 10 * CPU_REGISTER_SIZE,
    CPU_REG_11 = 11 * CPU_REGISTER_SIZE,
    CPU_REG_12 = 12 * CPU_REGISTER_SIZE,
    CPU_REG_13 = 13 * CPU_REGISTER_SIZE,
    CPU_REG_14 = 14 * CPU_REGISTER_SIZE,
    CPU_REG_15 = 15 * CPU_REGISTER_SIZE,

    // Register shorthands

    /// @brief Reference the stack pointer
    CPU_SP = (CPU_REGISTER_COUNT - 3) * CPU_REGISTER_SIZE,

    /// @brief References the link register
    CPU_LR = (CPU_REGISTER_COUNT - 2) * CPU_REGISTER_SIZE,

    /// @brief Reference the program counter
    CPU_PC = (CPU_REGISTER_COUNT - 1) * CPU_REGISTER_SIZE,
} CPU_Register;

static CPU_Register CPU_RegNameToAddr (uint8_t regName) {
    switch (regName) {
        case 0: return CPU_REG_0;
        case 1: return CPU_REG_1;
        case 2: return CPU_REG_2;
        case 3: return CPU_REG_3;
        case 4: return CPU_REG_4;
        case 5: return CPU_REG_5;
        case 6: return CPU_REG_6;
        case 7: return CPU_REG_7;
        case 8: return CPU_REG_8;
        case 9: return CPU_REG_9;
        case 10: return CPU_REG_10;
        case 11: return CPU_REG_11;
        case 12: return CPU_REG_12;
        case 13: return CPU_REG_13;
        case 14: return CPU_REG_14;
        case 15: return CPU_REG_15;
           
        // TODO: Find a way to deal with invalid reg name
        default: return CPU_REG_0;
    }
}

enum CPU_Flag {
    CPU_HALT = 1 << 0, // True if running
    
    CPU_P    = 1 << 1, // Parity flag
    CPU_Z    = 1 << 2, // Zero flag
    
    CPU_S    = 1 << 3, // Sign flag
    CPU_N    = 1 << 3, // Negative flag
};

/// CPU structure
typedef struct CPU {
    /// @brief Program counter
    programCounter_t pc;

    /// @brief The CPU register
    Memory registers;

    /// @brief The CPU stack
    Memory stack;

    /// @brief Pointer to the top of the stack
    memoryAddress_t stackPointer;

    /// @brief The CPU flags
    cpuFlag_t flag;

    /// @brief The CPU debug status
    bool debug;

    /// @brief The CPU endian
    uint32_t endian;
} CPU;

/// Init CPU struct
void CPU_Init (CPU * cpu, memoryAddress_t stackSize);

/// Free resource used by the virtual CPU
void CPU_Free (CPU * cpu);

/// Start the CPU cycle
bool CPU_Run (CPU * cpu, Memory * mem);

/// Set value to register
bool CPU_SetRegister (CPU * cpu, CPU_Register tReg, registerVal_t value);

/// Get value from register
bool CPU_GetRegister (CPU * cpu, CPU_Register sReg, registerVal_t * value);

/// @brief Set sequential bytes to target register
/// @param cpu CPU context
/// @param tReg Target register
/// @param buffer Buffer to copy
/// @param bufferSize Length of buffer
/// @return True on success copy, otherwise false
bool CPU_SetRegisterSeq (CPU * cpu, CPU_Register tReg, byte_t * buffer, memoryAddress_t bufferSize);

/// Convert uint32 big endian to little endian
uint32_t CPU_ToLittleEndian (uint32_t n);

/// Convert uint32 little endian to big endian
uint32_t CPU_ToBigEndian (uint32_t n);

/// @brief Convert virtual CPU endian to host endian
/// @param virtualCpuEndian
/// @param n 
/// @return 
uint32_t CPU_ToHostEndian (int virtualCpuEndian, uint32_t n);

/// @brief Convert host endian to virtual CPU endian
/// @param virtualCpuEndian
/// @param n 
/// @return 
uint32_t CPU_ToVirtualCpuEndian (int virtualCpuEndian, uint32_t n);

/// @brief Push value in source register to the stack and increment the stack pointer
/// @param cpu CPU context
/// @param sReg Source register
/// @return true on success
bool CPU_PushStack (CPU * cpu, CPU_Register sReg);

/// @brief Pop value from the stack to the target register and decrement the stack pointer
/// @param cpu CPU context
/// @param tReg Target register
/// @return true on success
bool CPU_PopStack (CPU * cpu, CPU_Register tReg);

/// @brief Copy data from memory to register
/// @param cpu CPU context
/// @param mem Memory context
/// @param memSrc Memory source address
/// @param tReg Target register
/// @param dataSize Data size
/// @return true on success
bool CPU_CopyMemToReg (CPU * cpu, Memory * mem, memoryAddress_t memSrc, register_t tReg, size_t dataSize);

/// @brief Copy value between 2 register
/// @param cpu CPU context
/// @param sReg Source register
/// @param tReg Target register
/// @return 
bool CPU_CopyBetweenRegister (CPU * cpu, register_t sReg, register_t tReg);