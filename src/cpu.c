#include "cpu.h"
#include "utils.h"
#include "instructions.h"
#include "command.h"
#include "byteUnit.h"

#define PRINT_INSTRUCTION_ON_ERROR
// #define DEBUG_ENDIAN_TEST

/*
 * Example:
 * 12 0f 00 00 de ad be ef ; load mem[0xdeadbeef], r15
 * First byte is always the op code.
 * Last 4 byte is the address.
 * Removing the op code and the address, we are left with
 * 0f 00 00
 * 
 * To get the register 0xff, we need to mask the raw data with
 * RAW_REGISTER_ADDRESS_MASK
 */
#define RAW_REGISTER_ADDRESS_MASK 0xF00000

#ifndef CPU_ARCHITECTURE // If CPU architecture is not defined, assume its 32bit
#define CPU_STACK_ENTRY_SIZE (4 * BYTE)
#define CPU_REGISTER_DATA_SIZE (4 * BYTE)
#elif CPU_ARCHITECTURE == 32 // If CPU architecture is 32bit, set storage to 32bit
#define CPU_STACK_ENTRY_SIZE (4 * BYTE)
#define CPU_REGISTER_DATA_SIZE (4 * BYTE)
#elif CPU_ARCHITECTURE == 64 // If CPU architecture is 64bit, set storage to 64bit
#define CPU_STACK_ENTRY_SIZE (8 * BYTE)
#define CPU_REGISTER_DATA_SIZE (8 * BYTE)
#endif

void CPU_Init (CPU * cpu, memoryAddress_t stackSize) {
    memset(cpu, 0, sizeof(CPU));
    
    Memory_Init(&cpu->registers, sizeof(byte_t) * CPU_REGISTER_SIZE * CPU_REGISTER_COUNT);
    Memory_Init(&cpu->stack, stackSize * CPU_STACK_ENTRY_SIZE);
    setBitmask8(&cpu->flag, CPU_HALT);

    cpu->stackPointer = 0;

    // Check CPU endian
    {
        // Set bytes
        cpu->registers.bytes[0] =
        cpu->registers.bytes[1] =
        cpu->registers.bytes[2] = 0;
        cpu->registers.bytes[3] = 1;
        
        uint32_t i = 0;
        Memory_GetInt32(&cpu->registers, 0, &i, 0);

        if (i == 1) {
            cpu->endian = LITTLE_ENDIAN;
        } else {
            cpu->endian = BIG_ENDIAN;
        }

        // Reset r0
        CPU_SetRegister(cpu, CPU_REG_0, 0);

#ifdef DEBUG_ENDIAN_TEST
        printf("%d\n", i);
        printf("System endian: %d\n", endian());
        printf("%d\n", cpu->endian);
        CPU_SetRegister(cpu, CPU_REG_0, 4);
        CPU_GetRegister(cpu, CPU_REG_0, &i);
        printf("%x\n", i);
        Memory_PrintMem(cpu->registers.bytes, cpu->registers.size);
#endif
    }
}

void CPU_Free (CPU * cpu) {
    Memory_Free(&cpu->registers);
    setBitmask8(&cpu->flag, CPU_HALT);
}

/// Fetch the next instruction. Returns true when instruction is not CPU_I_HALT
bool CPU_Fetch (CPU * cpu, Memory * mem, byte_t * opCode, byte_t * data) {
    bool cpuContinue = true;
    if (!Memory_Get(mem, cpu->pc, opCode)) {
        printf("Error fetching instruction: %s\n", "Index out of range");
        return false;
    }
    
    cpuContinue = (*opCode) != CPU_I_HALT_SET;
    
    if (data != NULL) {
        if (!Memory_GetSeq(mem, cpu->pc + 1, cpu->pc + 3, data)) {
            printf("Error fetching instruction: %s\n", "Index out of range");
            return false;
        }
        return cpuContinue;
    }
    
    return cpuContinue;
}

uint32_t CPU_ByteArrayToInt (byte_t * data, uint32_t size) {
    instruction_t out = 0;
    for (uint8_t i = 0; i < size; ++i) {
        out <<= 8;
        out  += data[i];
    }
    
    return out;
}

uint32_t CPU_ToLittleEndian (uint32_t n) {
    return (int)ntohl(n);
}

uint32_t CPU_ToBigEndian (uint32_t n) {
    return (int)htonl(n);
}

bool CPU_ParseDataSource (instruction_t rawData, CPU_DecodeInstruction * decoded, programCounter_t pc) {
    switch (decoded->sourceMode) {
        case CPU_I_SOURCE_PTR:
            decoded->data.sRegister = CPU_RegNameToAddr((rawData & 0x000F));
            decoded->data.sCopyByteCount = (rawData & 0x00F0) >> 4;
            break;
        case CPU_I_SOURCE_REG:
            decoded->data.sRegister = CPU_RegNameToAddr((rawData & 0xF000) >> 12);
            break;
        case CPU_I_SOURCE_IMM:
            decoded->data.sImmediate = (rawData & 0xFFFF);
            break;
        case CPU_I_SOURCE_MEM:
            decoded->data.sMemAddr = 0;
            break;
        case CPU_I_SOURCE_IMM_DWORD_QWORD:
            decoded->data.sImmediate = (rawData & 0x00F0);
            decoded->data.sMemAddr = pc + INSTRUCTION_SIZE;
            break;
            
        default:
            printf("Decode data failed. Source mode not implemented\n");
            return false;
    }

    return true;
}

bool CPU_DecodeData (instruction_t rawData, CPU_DecodeInstruction * decoded, programCounter_t pc) {
    decoded->sourceMode = (rawData & 0xF0000) >> 16;
    
    return CPU_ParseDataSource(rawData, decoded, pc);
}

bool CPU_DecodeStackOperations (byte_t data[DATA_SIZE], CPU_DecodeInstruction * decoded, programCounter_t pc) {
    decoded->sourceMode = CPU_I_SOURCE_REG;
    instruction_t rawData = CPU_ByteArrayToInt(data, DATA_SIZE);

    if (!CPU_ParseDataSource(rawData, decoded, pc)) {
        printf("CPU_DecodeStackOperations failed.");
        return false;
    }

    return true;
}

bool CPU_DecodeCoroutineSet(byte_t data[DATA_SIZE], CPU_DecodeInstruction * decoded, programCounter_t pc) {
    bool dataDecodeSuccess = false;
    const uint32_t subCommand = data[0] >> 4;
    const uint32_t sourceMode = data[0] & 0x0F;
    const CPU_CoroutineOperationsFlag sourceModeToFlag[3] = {
        CPU_COROUTINE_POP,
        CPU_COROUTINE_PUSH,
        CPU_COROUTINE_RETURN,
    };
    
    if (subCommand == 0) {
        decoded->flag = sourceModeToFlag[sourceMode];
        
        dataDecodeSuccess = CPU_DecodeStackOperations(data, decoded, pc);

        if (sourceMode == 0 || sourceMode == 2)
            decoded->targetRegister = decoded->data.sRegister;
    } else {
        decoded->flag = CPU_COROUTINE_FUNC;
        decoded->sourceMode = sourceMode;

        dataDecodeSuccess = CPU_DecodeData(CPU_ByteArrayToInt(data, DATA_SIZE), decoded, pc);
        decoded->targetRegister = CPU_PC;
    }

    if (!dataDecodeSuccess) {
        printf("CPU_DecodeCoroutineSet failed.");
    }
    
    return dataDecodeSuccess;
}

bool CPU_Decode (CPU * cpu, byte_t opcode, byte_t data[DATA_SIZE], CPU_DecodeInstruction * decoded) {
    decoded->opcode = opcode;
    
    // load & lnot
    instruction_t rawData = CPU_ByteArrayToInt(data, DATA_SIZE);
    bool decodedDataIsValid = true;
    
    bool isDataProc = opcode >= CPU_I_LOAD && opcode <= CPU_I_XOR;
    bool isJump = opcode >= CPU_I_JMP && opcode <= CPU_I_JMP_0;
    bool isControl = opcode >= CPU_I_GETC && opcode <= CPU_I_HALT_SET;
    bool isCoroutineSet = opcode == CPU_I_COROUTINE_SET;
    
    // Case 1: Coroutine set
    if (isCoroutineSet) {
        decodedDataIsValid = CPU_DecodeCoroutineSet(data, decoded, cpu->pc);
    }

    // Case 2: Data Loading & data processing
    else if (isDataProc || isJump) {
        decoded->targetRegister = CPU_RegNameToAddr((rawData & RAW_REGISTER_ADDRESS_MASK) >> 20);
        decodedDataIsValid = CPU_DecodeData(rawData, decoded, cpu->pc);
    }
    
    // Case 3: control instructions
    else if (isControl) {
        // Get the print mode and set the print mode into the target register
        // TODO: Find a better way to do this
        decoded->targetRegister = (rawData & RAW_REGISTER_ADDRESS_MASK) >> 20;
        decoded->data.sRegister = CPU_RegNameToAddr(rawData & 0xF);
    }
    
    // Fetch memory address
    if (decoded->sourceMode == CPU_I_SOURCE_MEM) {
        printf("CPU_I_SOURCE_MEM Unimplemented");
        cpu->pc += INSTRUCTION_SIZE;
    }
    
    return decodedDataIsValid;
}

bool CPU_Execute (CPU * cpu, Memory * mem, CPU_DecodeInstruction * decoded) {
    return Command_Execute(cpu, mem, decoded);
}

void CPU_PrintInstruction (byte_t opCode, byte_t * data) {
    CPU_PrintInstructionName(opCode);
    Memory_PrintMem(data, DATA_SIZE);
}

void CPU_PrintInstructionOnError (CPU* cpu, byte_t opCode, byte_t * data) {
#ifdef PRINT_INSTRUCTION_ON_ERROR
    printf("PC: 0x%04x\n", cpu->pc);
    CPU_PrintInstruction(opCode, data);
#endif
}

bool CPU_Run (CPU * cpu, Memory * mem) {
    unsetBitmask8(&cpu->flag, CPU_HALT);
    
    byte_t opcode;
    byte_t data[DATA_SIZE];
    CPU_DecodeInstruction decodedInstruction;
    while ( CPU_Fetch(cpu, mem, &opcode, data) ) {
        if (cpu->debug) {
            CPU_PrintInstruction(opcode, &data[0]);
        }

        const programCounter_t prevPc = cpu->pc;

        if (!CPU_Decode(cpu, opcode, data, &decodedInstruction)) {
            printf("CPU Decode error: %s\n", "Invalid instruction passed");
            CPU_PrintInstructionOnError(cpu, opcode, &data[0]);
            return false;
        }
        
        if (!CPU_Execute(cpu, mem, &decodedInstruction)) {
            printf("CPU Execute error: -\n");
            CPU_PrintInstructionOnError(cpu, opcode, &data[0]);
            return false;
        }

        // Only increment PC if the value has not been modified
        // If PC value at the start of the cycle is different than the value at the
        // end of the cycle, assume that it is the target PC that the program
        // desires and don't increment my instruction size.
        if (prevPc == cpu->pc) {
            cpu->pc += INSTRUCTION_SIZE;
        }
    }
    
    return true;
}

bool CPU_SetRegister (CPU * cpu, CPU_Register tReg, registerVal_t value) {
    if (!Memory_SetInt32(&cpu->registers, (uint8_t)tReg, value, cpu->endian)) {
        return false;
    }

    // Once register value has been set, set the CPU access variable
    if (tReg == CPU_PC) {
        cpu->pc = value;
        return true;
    } else if (tReg == CPU_SP) {
        cpu->stackPointer = value;
        return true;
    }

    return true;
}

bool CPU_GetRegister (CPU * cpu, CPU_Register sReg, registerVal_t * value) {
    if (sReg == CPU_PC) {
        *value = cpu->pc;
        return CPU_SetRegister(cpu, sReg, cpu->pc);
    } else if (sReg == CPU_SP) {
        *value = cpu->stackPointer;
        return CPU_SetRegister(cpu, sReg, cpu->stackPointer);
    }

    return Memory_GetInt32(&cpu->registers, (uint8_t)sReg, value, cpu->endian);
}

bool CPU_SetRegisterSeq (CPU * cpu, CPU_Register tReg, byte_t * buffer, memoryAddress_t bufferSize) {
    if (!Memory_SetLengthTrunc(&cpu->registers, (uint8_t)tReg, buffer, bufferSize)) {
        return false;
    }

    // If we're referencing the program counter or stack pointer, reread data back to CPU instance
    if (tReg == CPU_PC) {
        return Memory_GetInt32(&cpu->registers, (uint8_t)tReg, &cpu->pc, cpu->endian);
    } else if (tReg == CPU_SP) {
        return Memory_GetInt32(&cpu->registers, (uint8_t)tReg, &cpu->stackPointer, cpu->endian);
    }

    return true;
}

bool CPU_GetRegisterSeq (CPU * cpu, CPU_Register sReg, byte_t * buffer, memoryAddress_t bufferSize) {
    if (!Memory_GetSeq(&cpu->registers, sReg, sReg + CPU_REGISTER_DATA_SIZE, buffer)) {
        return false;
    }

    // If we're referencing the program counter or stack pointer, reread data back to CPU instance
    if (sReg == CPU_PC) {
        return Memory_GetInt32(&cpu->registers, (uint8_t)sReg, &cpu->pc, cpu->endian);
    } else if (sReg == CPU_SP) {
        return Memory_GetInt32(&cpu->registers, (uint8_t)sReg, &cpu->stackPointer, cpu->endian);
    }

    return true;
}

// TODO: We can probably simplify this function
uint32_t CPU_ToHostEndian (int virtualCpuEndian, uint32_t n) {
    if (hostByteOrder() == BIG_ENDIAN) {
        return CPU_ToBigEndian(n);
    }

    if (virtualCpuEndian == BIG_ENDIAN && hostByteOrder() == LITTLE_ENDIAN) {
        return CPU_ToLittleEndian(n);
    }

    return n;
}

// TODO: We can probably simplify this function
uint32_t CPU_ToVirtualCpuEndian (int virtualCpuEndian, uint32_t n) {
    if (virtualCpuEndian == LITTLE_ENDIAN && hostByteOrder() == BIG_ENDIAN) {
        return CPU_ToLittleEndian(n);
    }

    if (virtualCpuEndian == BIG_ENDIAN && hostByteOrder() == LITTLE_ENDIAN) {
        return CPU_ToBigEndian(n);
    }

    return n;
}

bool CPU_PushStack (CPU * cpu, CPU_Register sReg) {
    char errorTitle[] = "PushStack failure: %s\n";
    byte_t registerValue[CPU_STACK_ENTRY_SIZE];

    // Read value from reg
    if (!CPU_GetRegisterSeq(cpu, sReg, &registerValue[0], CPU_STACK_ENTRY_SIZE)) {
        printf(errorTitle, "Failed to get value from source register");
        return false;
    }

    // Write to the top of the stack
    if (!Memory_SetSeq(&cpu->stack, cpu->stackPointer + CPU_STACK_ENTRY_SIZE, &registerValue[0], CPU_STACK_ENTRY_SIZE)) {
        printf(errorTitle, "Failed to write value to stack");
        return false;
    }

    // Increment stack pointer
    if (!CPU_SetRegister(cpu, CPU_SP, cpu->stackPointer + CPU_STACK_ENTRY_SIZE)) {
        printf(errorTitle, "Failed to update stack pointer");
        return false;
    }

    cpu->stack.used += CPU_STACK_ENTRY_SIZE; // Manually increment stack usage
    return true;
}

bool CPU_PopStack (CPU * cpu, CPU_Register tReg) {
    char errorTitle[] = "PopStack failure: %s\n";

    if (cpu->stackPointer == 0) {
        printf(errorTitle, "Already at the bottom of the stack");
        return false;
    }

    // Get value in stack
    byte_t registerValue[CPU_STACK_ENTRY_SIZE];
    const memoryAddress_t stackStartAddr = cpu->stackPointer;
    const memoryAddress_t stackEndAddr = stackStartAddr + CPU_STACK_ENTRY_SIZE - 1;
    if (!Memory_GetSeq(&cpu->stack, stackStartAddr, stackEndAddr, &registerValue[0])) {
        printf(errorTitle, "Failed to read from stack");
        return false;
    }

    // Write value to reg
    if (!CPU_SetRegisterSeq(cpu, tReg, &registerValue[0], CPU_STACK_ENTRY_SIZE)) {
        printf(errorTitle, "Failed to write value to register");
        return false;
    }

    // Decrement stack pointer
    if (!CPU_SetRegister(cpu, CPU_SP, cpu->stackPointer - CPU_STACK_ENTRY_SIZE)) {
        printf(errorTitle, "Failed to update stack pointer");
        return false;
    }

    cpu->stack.used -= CPU_STACK_ENTRY_SIZE; // Manually decrement stack usage
    return true;
}

bool CPU_CopyMemToReg (CPU * cpu, Memory * mem, memoryAddress_t memSrc, register_t tReg, size_t dataSize) {
    char errorTitle[] = "CPU_CopyMemToReg failed: %s\n";
    if (!Memory_Copy(mem, memSrc, &cpu->registers, tReg, dataSize)) {
        printf(errorTitle, "Memory_Copy failed");
        return false;
    }

    bool updateInternalSuccess = true;
    if (tReg == CPU_PC) {
        updateInternalSuccess = Memory_GetInt32(&cpu->registers, tReg, &cpu->pc, cpu->endian);
    } else if (tReg == CPU_SP) {
        updateInternalSuccess = Memory_GetInt32(&cpu->registers, tReg, &cpu->stackPointer, cpu->endian);
    }

    // In case we're coppying value between registers
    if (mem == &cpu->registers) {
        if (memSrc == CPU_PC) {
            updateInternalSuccess = Memory_GetInt32(&cpu->registers, memSrc, &cpu->pc, cpu->endian);
        } else if (memSrc == CPU_SP) {
            updateInternalSuccess = Memory_GetInt32(&cpu->registers, memSrc, &cpu->stackPointer, cpu->endian);
        }
    }

    if (!updateInternalSuccess) {
        printf(errorTitle, "Memory_GetInt32 failed");
    }

    return updateInternalSuccess;
}

bool CPU_CopyBetweenRegister (CPU * cpu, register_t sReg, register_t tReg) {
    if (!CPU_CopyMemToReg(cpu, &cpu->registers, sReg, tReg, CPU_REGISTER_SIZE)) {
        printf("CPU_CopyBetweenRegister failed: CPU_CopyBetweenRegister Failed");
        return false;
    }

    return true;
}