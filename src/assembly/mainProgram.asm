; This program starts by accepting user input. If user input is
; the ASCII character 0, it will terminate the program.
; If input is the ASCII character 1, then it will print "Hello World".
; If input is any other ASCII, then it will print the first 10
; Fibonacci sequence.

main:
	.mainStart:
	getc r0 ; Get user input
	
	; If r0 is ASCII 0, then exit
	load r1, r0
	sub r1, 48 ; ASCII 0
	jmp0 r1, .exitMain
	
	; If r0 is ASCII 1, then print Hello World
	load r1, r0
	sub r1, 49 ; ASCII 1
	jmp0 r1, .printHelloWorld
	
	.startFib:
	fun dword[fib]
	jmp dword[.mainStart]

	.printHelloWorld:
	fun dword[helloWorld]
	jmp dword[.mainStart]
	
	.exitMain:
	halt
	
helloWorld:
	load r0, 0x0a
	
	; Push l
	load r2, 0x6c ; l
	
	; Push o
	load r3, 0x6f ; o
	
	; Print H
	load r1, 0x48
	outc r1
	
	; Print e
	load r1, 0x65
	outc r1
	
	; Print l
	outc r2
	outc r2
	
	; Print o
	outc r3
	
	; Print [space]
	load r1, 0x20
	outc r1
	
	; Print W
	load r1, 0x57
	outc r1
	
	; Print o
	outc r3
	
	; Print r
	load r1, 0x72
	outc r1
	
	; Print l
	outc r2
	
	; Print d
	load r1, 0x64
	outc r1
	
	; Print newline
	outc r0
	
	ret

fib:
    load r0, 1 ; Fib counter
    load r2, 1 ; Prev fib value
    load r3, 1 ; Current fib
    
    .fibLoop:
        ; Special case for fib 1
        load r1, r0
        sub r1, 1
        jmp0 r1, .fibDone
        ; Special case for fib 2
        load r1, r0
        sub r1, 2
        jmp0 r1, .fibDone
        
        ; Normal case for fib. Calculate next value
        ; Next fib value is current + prev
        load r1, r3
        add r1, r2
        
        load r2, r3
        load r3, r1

        .fibDone:
            ; Increment r0
            add r0, 1

            ; Print fib
            outd r3
            load r1, 0x20 ; Space
            outc r1 ; Print space

        ; Break once we've calculated up to the 10th fib
        .fibLoopCheck:
            load r1, r0
            sub r1, 10
            jmp0 r1, .fibLoopDone
            jmp .fibLoop
    .fibLoopDone:
        load r1, 0x0A
        outc r1
        ret