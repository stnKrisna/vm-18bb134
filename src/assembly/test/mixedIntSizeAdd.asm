main:
	load r3, 10
		
	; Load 123 to memory address 0xaabbccdd
	load r0, 123
	load mem[0x000000f0], r0
	load r0, 0
	outd r0 ; Print 0
	outc r3 ; \n
	
	; Set first argument for coroutine to memory address 0xaabbccdd
	; And set second argument for coroutine to 321
	load r1, dword[0x000000f0]
	load r2, 321
	
	; Reload value from memory back to register 0
	load r0, ptr[r1, 4]
	
	; Debug print
	outd r0 ; 123
	outc r3 ; \n
	outd r1 ; 240
	outc r3 ; \n
	outd r2 ; 321
	outc r3 ; \n
	
	; Debug endian
	add r0, r2
	outd r0 ; 444
	outc r3 ; \n