fib:
    load r0, 1 ; Fib counter
    load r2, 1 ; Prev fib value
    load r3, 1 ; Current fib
    
    .fibLoop:
        ; Special case for fib 1
        load r1, r0
        sub r1, 1
        jmp0 r1, .fibDone
        ; Special case for fib 2
        load r1, r0
        sub r1, 2
        jmp0 r1, .fibDone
        
        ; Normal case for fib. Calculate next value
        ; Next fib value is current + prev
        load r1, r3
        add r1, r2
        
        load r2, r3
        load r3, r1

        .fibDone:
            ; Increment r0
            add r0, 1

            ; Print fib
            outd r3
            load r1, 0x20 ; Space
            outc r1 ; Print space

        ; Break once we've calculated up to the 10th fib
        .fibLoopCheck:
            load r1, r0
            sub r1, 10
            jmp0 r1, .fibLoopDone
            jmp .fibLoop
    .fibLoopDone:
        load r1, 0x0A
        outc r1
        stop