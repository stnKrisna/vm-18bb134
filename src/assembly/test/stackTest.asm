main:
	; Push new line character
	load r6, 10
	
	; Push initial data to register r5
	load r5, 8385
	
	; Store r5 to stack & replace r5 with 4921
	push r5 ; 8385 in stack
	load r5, 4921
	
	; Debug print
	outd r5 ; print 4921
	outc r5 ; print \n
	outd SP ; print SP
	outc r5 ; print \n
	outd PC ; print PC
	outc r5 ; print \n
	
	; Restore r5's initial value
	pop r5
	
	; Debug print
	outd r5 ; print 8385
	outc r5 ; print \n
	outd SP ; print SP
	outc r5 ; print \n
	outd PC ; print PC
	outc r5 ; print \n
	halt