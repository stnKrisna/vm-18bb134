main:
	; Push address of exit to PC
	load r0, dword[exit]
	load mem[0x000000f0], r0
	load r1, dword[0x000000f0]
	load r1, ptr[r1, 4]
	load PC, r1
	
	; Push new line character
	load r6, 10
	
	; Push initial data to register r5
	load r5, 8385
	
	; Store r5 to stack & replace r5 with 4921
	push r5 ; 8385 in stack
	load r5, 4921
	
	; Debug print
	outd r5 ; print 4921
	outc r6 ; print \n
	outd SP ; print SP
	outc r6 ; print \n
	outd PC ; print PC
	outc r6 ; print \n
	
	; Restore r5's initial value
	pop r5
	
	; Debug print
	outd r5 ; print 8385
	outc r6 ; print \n
	outd SP ; print SP
	outc r6 ; print \n
	outd PC ; print PC
	outc r6 ; print \n

exit:
	load r0, 0x78
	outc r0 ; Print character 'x'
	halt