main:
	; Load 123 to memory address 0xaabbccdd
	load r0, 123
	load mem[0x000000f0], r0
	
	; Set first argument for coroutine to memory address 0x000000f0
	; And set second argument for coroutine to 321
	load r1, dword[0x000000f0]
	load r2, 321
	
	; Begin coroutine
	fun dword[add] ; LR should be pushed to stack
				   ; LR should be set to address of the next instruction
				   ; PC should be set to dword[add]
	
	; Print result
	outd r0 ; Should print 444
	
	; Print newline
	load r4, 0x0a ; newline
	outc r4
	
	halt

add:
	push r3
	push r4
	
	load r3, 0x4a ; J
	load r4, 0x0a ; newline
	
	outc r3
	outc r4
	
	pop r4
	pop r3
	
	; Load value stored in address pointed by r1 to r0
	load r0, ptr[r1]
	
	; And add whatever value is stored in r2 with r0
	add r0, r2

	ret ; Set PC to value in LR
		; Pop LR