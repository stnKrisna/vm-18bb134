; Use this to compile: https://hlorenzi.github.io/customasm/web/

#subruledef register
{
	; General purpose register
    r0  => 0x0 ; When invoking a subroutine, r0 will be used to store the return value
	 
	; When invoking a subroutine, r1 to r3 can be used to
	; pass argument to subroutine
    r1  => 0x1
    r2  => 0x2
    r3  => 0x3
	 
    r4  => 0x4
    r5  => 0x5
    r6  => 0x6
    r7  => 0x7
	r8  => 0x8
	r9  => 0x9
	r10 => 0xa
	r11 => 0xb
	r12 => 0xc

	r13 => 0xd ; Stack pointer
	r14 => 0xe ; Link register
	r15 => 0xf ; Program counter
	
	SP => 0xd
	LR => 0xe
	PC => 0xf
}

#subruledef source
{
    {r: register} => 0x0 @ r @ 0x000 ; register always 4 bits long
    {immediate: i16}   => 0x1 @ immediate
	
	; ptr have optional byte count to copy
    ptr[{r: register}] => 0x3 @ 0x00 @ 0x8 @ r ; Copy max byte a register can hold. Set default to 8 for future proofing
    ptr[{r: register}, {size: i4}] => 0x3 @ 0x00 @ size @ r
    
    ; Diff between register and pointer mode:
    ; register: Take value from the register as-is
    ; ptr: Take value from memory based on address stored in the register
    
    ; When using direct memory access, load the next 4 bytes and increment pc by 4
    mem[{address: i32}] => 0x20 @ 0x00 @ address ; Raw mem address
	
	; Load 32bit int, load the next 4 bytes and increment pc by 4
	dword[{dword: i32}] => 0x40 @ 0x000 @ dword
	
	; Load 64bit int, load the next 8 bytes and increment pc by 8
	qword[{qword: i64}] => 0x40 @ 0x010 @ qword
}

#ruledef
{
    ; Rules for data loading
    ; opcode (4 bits)
    ; opcode_condition (4 bits)
    ; source (8 bits)
    load  {r: register}, {src: source} => 0x1 @ 0x0 @ r @ src
    loadv {r: register}, {immediate}   => 0x1 @ 0x0 @ r @ 0x1 @ immediate`16
    lnot  {r: register}, {src: source} => 0x1 @ 0x1 @ r @ src
	
	; Loading register content to memory
	; opcode (4 bits)
	; unused (8 bits)
	; source register (4 bits)
	load mem[{address: i32}], {r: register} => 0x1 @ 0x2 @ 0x00 @ r @ 0x000 @ address`32

    ; Rules for data processing
    ; opcode (4 bits)
    ; opcode_condition (4 bits)
    ; source (8 bits)
    add  {r: register}, {src: source} => 0x20 @ r @ src
    sub  {r: register}, {src: source} => 0x21 @ r @ src
    mul  {r: register}, {src: source} => 0x22 @ r @ src
    div  {r: register}, {src: source} => 0x23 @ r @ src
    mod  {r: register}, {src: source} => 0x24 @ r @ src
    and  {r: register}, {src: source} => 0x30 @ r @ src
    or   {r: register}, {src: source} => 0x31 @ r @ src
    xor  {r: register}, {src: source} => 0x32 @ r @ src
 
    ; Rules for jumping
    ; opcode (4 bits)
    ; opcode_condition (4 bits)
    ; source (8 bits)
    jmp   {src: source}  => 0xe  @ 0x00 @ src
    jmp0  {rd: register}, {src: source} => 0xef @ rd @ src
    jmpn0 {rd: register}, {src: source} => 0xee @ rd @ src
 
    ; Controls
    getc {r: register} => 0xf000000 @ r ; Get char from terminal and store to register
    outd {r: register} => 0xf110000 @ r ; Print whatever value stored in the register as int to terminal
    outc {r: register} => 0xf100000 @ r ; Print whatever value stored in the register as char to terminal
    puts {r: register} => 0xf200000 @ r ; Print null-terminated string to terminal. Value of register expected to be mem address
    putb {r: register} => 0xf210000 @ r ; Print null-terminated byte to terminal. Value of register expected to be mem address
    inc  {r: register} => 0xf300000 @ r ; Get character from terminal, store to register and print the char
	
	;Coroutine stuff
	; opcode (4 bits)
    ; opcode_condition (4 bits)
    ; unused (4 bits)
    ; register (4 bits)
	pop {r: register}  => 0xfd @ 0x00 @ r @ 0x000 ; Store value to reg, and pop stack
	push {r: register} => 0xfd @ 0x01 @ r @ 0x000 ; Push reg to stack
	fun {src: source}  => 0xfd @ 0xf @ src ; Start coroutine
	ret                => 0xfd @ 0x02 @ 0x0000
	ret {r: register}  => 0xfd @ 0x02 @ r @ 0x000 ; Store value to reg, and pop stack
	
	; Jump using memory pointer. Need to add special padding
	; opcode (4 bits)
    ; opcode_condition (4 bits)
	; unused (8 bits)
	funm mem[{address: i32}]  => 0xfd @ 0xf20000 @ address`32 ; Start coroutine using value in memory
	
	; Misc.
    dbgr  => 0xfee10bad ; tell CPU to enter debugger
    halt  => 0xfee10bed ; halt the program (stop execution but dont exit interpreter)
    stop  => 0xfee1dead ; halt the vm (tell interpreter to terminate)
    noop  => 0x00000000 ; do nothing. Go to next instruction
}