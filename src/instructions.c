#include "instructions.h"
#include "utils.h"

char * getOpName (CPU_Instructions opCode) {
    switch (opCode)
    {
        case CPU_I_LOAD:
            return "LOAD";
        case CPU_I_LNOT:
            return "LNOT";
        case CPU_I_ADD:
            return "ADD";
        case CPU_I_SUB:
            return "SUB";
        case CPU_I_MUL:
            return "MUL";
        case CPU_I_DIV:
            return "DIV";
        case CPU_I_MOD:
            return "MOD";
        case CPU_I_AND:
            return "AND";
        case CPU_I_OX:
            return "OX";
        case CPU_I_XOR:
            return "XOR";
        case CPU_I_JMP:
            return "JMP";
        case CPU_I_JMP_0:
            return "JMP_0";
        case CPU_I_JMP_N0:
            return "JMP_N0";
        case CPU_I_GETC:
            return "GETC";
        case CPU_I_OUTC:
            return "OUTC";
        case CPU_I_PUTS:
            return "PUTS";
        // case CPU_I_PUTB: break;
        case CPU_I_INC:
            return "INC";
        case CPU_I_COROUTINE_SET:
            return "COROUTINE_SET";
        case CPU_I_HALT_SET:
            return "HALT_SET";
        case CPU_I_NOOP:
            return "NOOP";
        case CPU_I_LOADM:
            return "LOADM";
    
    default:
        return "UNKNOWN";
    }
}

void CPU_PrintInstructionName (CPU_Instructions opCode) {
    printf("op: %2x (%s)\n", opCode, getOpName(opCode));
}