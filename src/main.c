#include <stdio.h>

#include "vm.h"

void main_LoadProgram (Memory * mem, bool dumpMemory) {
    const byte_t program[] = {
        /* 0x00 */ 0xf0, 0x00, 0x00, 0x00, 0x10, 0x10, 0x00, 0x00, 0x21, 0x11, 0x00, 0x30, 0xef, 0x11, 0x00, 0x3c, 
        /* 0x10 */ 0x10, 0x10, 0x00, 0x00, 0x21, 0x11, 0x00, 0x31, 0xef, 0x11, 0x00, 0x2c, 0xfd, 0xf4, 0x00, 0x00, 
        /* 0x20 */ 0x00, 0x00, 0x00, 0x98, 0xe0, 0x04, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xfd, 0xf4, 0x00, 0x00, 
        /* 0x30 */ 0x00, 0x00, 0x00, 0x40, 0xe0, 0x04, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xfe, 0xe1, 0x0b, 0xed, 
        /* 0x40 */ 0x10, 0x01, 0x00, 0x0a, 0x10, 0x21, 0x00, 0x6c, 0x10, 0x31, 0x00, 0x6f, 0x10, 0x11, 0x00, 0x48, 
        /* 0x50 */ 0xf1, 0x00, 0x00, 0x01, 0x10, 0x11, 0x00, 0x65, 0xf1, 0x00, 0x00, 0x01, 0xf1, 0x00, 0x00, 0x02, 
        /* 0x60 */ 0xf1, 0x00, 0x00, 0x02, 0xf1, 0x00, 0x00, 0x03, 0x10, 0x11, 0x00, 0x20, 0xf1, 0x00, 0x00, 0x01, 
        /* 0x70 */ 0x10, 0x11, 0x00, 0x57, 0xf1, 0x00, 0x00, 0x01, 0xf1, 0x00, 0x00, 0x03, 0x10, 0x11, 0x00, 0x72, 
        /* 0x80 */ 0xf1, 0x00, 0x00, 0x01, 0xf1, 0x00, 0x00, 0x02, 0x10, 0x11, 0x00, 0x64, 0xf1, 0x00, 0x00, 0x01, 
        /* 0x90 */ 0xf1, 0x00, 0x00, 0x00, 0xfd, 0x02, 0x00, 0x00, 0x10, 0x01, 0x00, 0x01, 0x10, 0x21, 0x00, 0x01, 
        /* 0xa0 */ 0x10, 0x31, 0x00, 0x01, 0x10, 0x10, 0x00, 0x00, 0x21, 0x11, 0x00, 0x01, 0xef, 0x11, 0x00, 0xcc, 
        /* 0xb0 */ 0x10, 0x10, 0x00, 0x00, 0x21, 0x11, 0x00, 0x02, 0xef, 0x11, 0x00, 0xcc, 0x10, 0x10, 0x30, 0x00, 
        /* 0xc0 */ 0x20, 0x10, 0x20, 0x00, 0x10, 0x20, 0x30, 0x00, 0x10, 0x30, 0x10, 0x00, 0x20, 0x01, 0x00, 0x01, 
        /* 0xd0 */ 0xf1, 0x10, 0x00, 0x03, 0x10, 0x11, 0x00, 0x20, 0xf1, 0x00, 0x00, 0x01, 0x10, 0x10, 0x00, 0x00, 
        /* 0xe0 */ 0x21, 0x11, 0x00, 0x0a, 0xef, 0x11, 0x00, 0xec, 0xe0, 0x01, 0x00, 0xa4, 0x10, 0x11, 0x00, 0x0a, 
        /* 0xf0 */ 0xf1, 0x00, 0x00, 0x01, 0xfd, 0x02, 0x00, 0x00
    };

    const size_t programSize = sizeof(program);
    const size_t copyableBytes = programSize > mem->size
        ? mem->size
        : programSize;

    printf("Available Mem: %d Byte%c\n", mem->size, mem->size != 1 ? 's' : ' ');
    printf("Program Size: %lu Byte%c\n", sizeof(program), sizeof(program) != 1 ? 's' : ' ');
    printf("Copyable Bytes: %lu Byte%c\n", copyableBytes, copyableBytes != 1 ? 's' : ' ');
    
    memcpy(mem->bytes, program, sizeof(program));

    if (dumpMemory)
        Memory_PrintMem(mem->bytes, mem->size);
}

int main (int argc, char * argv[]) {
    VM vm = VM_NewVm(256, 256);
    
    main_LoadProgram(&vm.memory, false);
    CPU_Run(&vm.cpu, &vm.memory);
    
    if (vm.cpu.debug)
        Memory_PrintMem(vm.cpu.registers.bytes, vm.cpu.registers.size);

    VM_Free(&vm);
    
    printf("Press enter to continue...\n");
    getchar();
    
    return 0;
}
