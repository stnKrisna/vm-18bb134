#pragma once

#include "types.h"
#include "utils.h"

/// Memory struct
typedef struct Memory {
    byte_t * bytes;
    
    memoryAddress_t size;
    memoryAddress_t used;
} Memory;

/// Init memory struct. Memory bytes may contain garbage value
void Memory_Init (Memory * memory, memoryAddress_t virtualMemorySize);

/// Deallocate memory bytes
void Memory_Free (Memory * memory);

/// Get whatever is stored in the memory at location. Returns false when at is out of range
bool Memory_Get (Memory * memory, memoryAddress_t at, byte_t * content);

/// @brief Retreive a 32bit integer
/// @param memory Memory context
/// @param at Memory address
/// @param data Where to store the output
/// @param endian Virtual CPU endian
/// @return 
bool Memory_GetInt32 (Memory * memory, memoryAddress_t at, uint32_t * data, int endian);

/// Get content of memory by size
bool Memory_GetLength (Memory * memory, memoryAddress_t at, memoryAddress_t length, byte_t * buffer);

/// Get whatever is stored in the memory from start until end and copy it to the buffer. Make sure the buffer has enough space to hold (end - start) number of bytes. End must be at least (start + 1). Returns false when start or end is out of memory range
bool Memory_GetSeq (Memory * memory, memoryAddress_t start, memoryAddress_t end, byte_t * buffer);

/// @brief Set memory by data array
/// @param memory Memory context
/// @param storeAt Store at address
/// @param dataBuffer Buffer to copy
/// @param dataLength Length of buffer
/// @return 
bool Memory_SetSeq (Memory * memory, memoryAddress_t storeAt, byte_t * dataBuffer, memoryAddress_t dataLength);

/// @brief Set memory by data array. Truncate data if length is out of total memory pool
/// @param memory 
/// @param storeAt 
/// @param dataBuffer 
/// @param dataLength 
/// @return 
bool Memory_SetLengthTrunc (Memory * memory, memoryAddress_t storeAt, byte_t * dataBuffer, memoryAddress_t dataLength);

/// Store a byte. Returns true on success, returns false when index is out of range
bool Memory_Set (Memory * memory, memoryAddress_t at, byte_t data);

/// @brief Store a 32bit integer
/// @param memory Memory context
/// @param at Memory address
/// @param data 
/// @param endian Virtual CPU endian
/// @return 
bool Memory_SetInt32 (Memory * memory, memoryAddress_t at, uint32_t data, int endian);

/// Print byte array
void Memory_PrintMem (byte_t * bytes, size_t bytesCount);

/// @brief Copy data between memory
/// @param src Source memory context
/// @param sAddress Source address
/// @param dst Destination memory context
/// @param dAddr Destination address
/// @param size Data size to copy
/// @return True on success
bool Memory_Copy (Memory * src, memoryAddress_t sAddress, Memory * dst, memoryAddress_t dAddr, size_t size);

/// @brief Copy data between memory. Truncate data that overflow source or destination memory
/// @see Memory_Copy
/// @param src Source memory context
/// @param sAddress Source address
/// @param dst Destination memory context
/// @param dAddr Destination address
/// @param size Data size to copy
/// @return True on success
bool Memory_CopyTrunc (Memory * src, memoryAddress_t sAddress, Memory * dst, memoryAddress_t dAddr, size_t size);