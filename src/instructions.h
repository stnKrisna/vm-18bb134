#pragma once

#include "types.h"
#include "cpu.h"
#include "byteUnit.h"

#define OPCODE_SIZE      (1 * BYTE) // in bytes
#define DATA_SIZE        (INSTRUCTION_SIZE - OPCODE_SIZE) // in bytes

typedef enum CPU_InstructionsSource {
    /// @brief Data source from register
    CPU_I_SOURCE_REG = 0,
    
    /// @brief Data source immediate mode
    CPU_I_SOURCE_IMM = 1,
    
    /// @brief Data source from memory
    CPU_I_SOURCE_MEM = 2,
    
    /// @brief Data source from pointer
    CPU_I_SOURCE_PTR = 3,

    /// @brief Data source from DWORD or QWORD
    CPU_I_SOURCE_IMM_DWORD_QWORD = 4,
} CPU_InstructionsSource;

typedef struct CPU_DecodedData {
    /// @brief Address of source register
    CPU_Register    sRegister;

    /// @brief Immediate value
    immediateData_t sImmediate;

    /// @brief Source of memory address
    memoryAddress_t sMemAddr;

    /// @brief Number of byte to copy (ptr mode only)
    byte_t          sCopyByteCount;
} CPU_DecodedData;

typedef enum CPU_Instructions {
    // Data Loading
    CPU_I_LOAD = 0x10,
    CPU_I_LNOT = 0x11,
    CPU_I_LOADM = 0x12,
    
    // Data processing
    CPU_I_ADD  = 0x20,
    CPU_I_SUB  = 0x21,
    CPU_I_MUL  = 0x22,
    CPU_I_DIV  = 0x23,
    CPU_I_MOD  = 0x24,
    CPU_I_AND  = 0x30,
    CPU_I_OX   = 0x31,
    CPU_I_XOR  = 0x32,
    
    // Jumps
    /// Jump by source
    CPU_I_JMP = 0xe0,
    
    /// Jump if 0
    CPU_I_JMP_0  = 0xef,
    
    /// Jump if not 0
    CPU_I_JMP_N0 = 0xee,
    
    // Controls
    CPU_I_GETC = 0xf0,
    CPU_I_OUTC = 0xf1,
    CPU_I_PUTS = 0xf2,
    CPU_I_PUTB = 0xf2,
    CPU_I_INC  = 0xf3,
    
    // Sets of coroutine related instructions
    CPU_I_COROUTINE_SET  = 0xfd,

    // VM halting related instructions
    CPU_I_HALT_SET = 0xfe,

    CPU_I_NOOP = 0x00, // Do Nothing :)
} CPU_Instructions;

typedef struct CPU_DecodeInstruction {
    CPU_Instructions opcode;
    CPU_Register targetRegister;
    CPU_InstructionsSource sourceMode;
    CPU_DecodedData data;

    /// @brief Instruction flag
    byte_t flag;
} CPU_DecodeInstruction;

/// @brief Flags for coroutine instruction set
typedef enum CPU_CoroutineOperationsFlag {
    /// @brief CPU_DecodeInstruction flag for no flag
    CPU_COROUTINE_CLEAR  = 0b00000000,

    /// @brief CPU_DecodeInstruction flag for stack push
    CPU_COROUTINE_PUSH   = 0b00000001,

    /// @brief CPU_DecodeInstruction flag for stack pop
    CPU_COROUTINE_POP    = 0b00000010,

    /// @brief CPU_DecodeInstruction flag for function call
    CPU_COROUTINE_FUNC   = 0b00000100,

    /// @brief CPU_DecodeInstruction flag for return operation
    CPU_COROUTINE_RETURN = 0b00000110,
} CPU_CoroutineOperationsFlag;

/// Pretty print instruction name
void CPU_PrintInstructionName (CPU_Instructions opCode);