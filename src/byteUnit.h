#pragma once

#include "types.h"

#define BYTE (sizeof(byte_t))
#define KILOBYTE (BYTE * 1024)
#define MEGABYTE (KILOBYTE * 1024)
#define GIGABYTE (MEGABYTE * 1024)

#define DWORD (4 * BYTE)
#define QWORD (8 * BYTE)

#define INSTRUCTION_SIZE (4 * BYTE)