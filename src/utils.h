#pragma once

#include <math.h>
#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdbool.h>

static void* safe_mem_malloc(size_t n, unsigned long line) {
    void* p = malloc(n);
    if (!p) {
        fprintf(stderr, "[%s:%lu]Out of memory(%lu bytes)\n",
                __FILE__, line, (unsigned long)n);
        exit(EXIT_FAILURE);
    }
    return p;
}
#define safe_malloc(n) safe_mem_malloc(n, __LINE__)

static void safe_mem_free(void ** ptr) {
    if (ptr != NULL && *ptr != NULL) {
        free (*ptr);
        *ptr = NULL;
    }
}
#define safe_free(pointer) safe_mem_free((void **) &(pointer))

static void setBitmask8 (uint8_t * bitmask, uint8_t pos) {
    (*bitmask) |= pos;
}

static void setBitmask16 (uint16_t * bitmask, uint16_t pos) {
    (*bitmask) |= pos;
}

static void setBitmask32 (uint32_t * bitmask, uint32_t pos) {
    (*bitmask) |= pos;
}

static void unsetBitmask8 (uint8_t * bitmask, uint8_t pos) {
    (*bitmask) ^= (0 ^ (*bitmask)) & pos;
}

static void unsetBitmask16 (uint16_t * bitmask, uint16_t pos) {
    (*bitmask) ^= (0 ^ (*bitmask)) & pos;
}

static void unsetBitmask32 (uint32_t * bitmask, uint32_t pos) {
    (*bitmask) ^= (0 ^ (*bitmask)) & pos;
}

static bool isSetBitmask8 (uint8_t bitmask, uint8_t pos) {
    return (bitmask & pos) != 0;
}

static bool isSetBitmask16 (uint16_t bitmask, uint16_t pos) {
    return (bitmask & pos) != 0;
}

static bool isSetBitmask32 (uint32_t bitmask, uint32_t pos) {
    return (bitmask & pos) != 0;
}

/// Get host byte order
static int endian () {
    int i = 1;
    char *p = (char *)&i;

    if (p[0] == 1)
        return LITTLE_ENDIAN;
    else
        return BIG_ENDIAN;
}
