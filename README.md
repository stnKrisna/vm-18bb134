# VM-18BB134

## Build Instruction

1. **Create build directory**

    ```sh
    mkdir build build/xcode
    ```

    Or replace `xcode` for any other pipeline
2. **Create build pipeline**

    ```sh
    cd build/xcode; cmake -G Xcode ../../; cd ../../
    ```

    Or replace `Xcode` for any other pipeline
3. **Open the Xcode project**
    
    (or other pipeline), or, when using `make`, run the appropriate `make` command

## To-do

- [ ] support for `add`, `sub` and `mul` source from ptr
- [ ] support for `add`, `sub` and `mul` source from mem

- [ ] support for `mod` source from register
- [ ] support for `mod` immediate mode
- [ ] Prevent division by 0
- [ ] support for `div` and `mod` source from ptr
- [ ] support for `div` and `mod` source from mem

- [ ] support for `and`, `or` and `xor` source from register
- [ ] support for `and`, `or` and `xor` immediate mode
- [ ] support for `and`, `or`, `xor`, `load`, and `lnot` source from ptr
- [ ] support for `and`, `or`, `xor`, `load`, and `lnot` source from mem

- [ ] support for `jmp` and `jmpn0` source from register
- [ ] support for `jmp0` and `jmpn0` immediate mode
- [ ] support for `jmp`, `jmp0` and `jmpn0` source from ptr
- [ ] support for `jmp`, `jmp0` and `jmpn0` source from mem
